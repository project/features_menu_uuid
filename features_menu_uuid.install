<?php

/**
 * @file
 * Install / Update functions for Features Menu UUID.
 */

/**
 * Implements hook_install().
 */
function features_menu_uuid_install() {
  // Ensure the menu_links table has a UUID column.
  if (!db_field_exists('menu_links', 'uuid')) {
    $field_spec = array(
      'type' => 'char',
      'length' => 36,
      'not null' => TRUE,
      'default' => '',
      'description' => 'The Universally Unique Identifier.',
    );
    $indexes = array(
      'uuid' => array(
        'uuid',
      ),
    );
    db_add_field('menu_links', 'uuid', $field_spec, $indexes);
  }
}

/**
 * Implements hook_enable().
 */
function features_menu_uuid_enable() {
  drupal_set_message(t('If the process of installing the Features Menu UUID module times out, just disable it (not uninstall!) and enable it again to finish this required process.'));

  // Add a UUID for each menu item.
  $links = db_select('menu_links', 'ml')
    ->fields('ml', array('mlid'))
    ->condition('ml.uuid', '')
    // Order by weight so as to be helpful for menus that are only one level
    // deep.
    ->orderBy('weight')
    ->execute()
    ->fetchAll();

  foreach ($links as $record) {
    db_update('menu_links')
      ->fields(array('uuid' => ctools_uuid_generate()))
      ->condition('mlid', $record->mlid)
      ->execute();
  }

  drupal_set_message(t('All menu items have been given a UUID, so they can now be exported as intended.'));
}

/**
 * Implements hook_install().
 */
function features_menu_uuid_uninstall() {
  if (!module_exists('entity_menu_links')) {
    db_drop_field('menu_links', 'uuid');
    drupal_set_message(t('The "uuid" field from the "menu_links" table has been removed.'));
  }
  else {
    drupal_set_message(t('The "uuid" field from the "menu_links" table has not been removed because it is still needed by the Entity Menu Links module.'));
  }
}

/**
 * Implements hook_schema_alter().
 */
function features_menu_uuid_schema_alter(&$schema = array()) {
  // A standard definition for the UUID field.
  $schema['menu_links']['fields']['uuid'] = array(
    'type' => 'char',
    'length' => 36,
    'not null' => TRUE,
    'default' => '',
    'description' => 'The Universally Unique Identifier.',
  );

  // Make the UUID field unique.
  $schema['menu_links']['unique keys']['menu_links_uuid_key'] = array(
    'uuid',
  );
}

/**
 * Implementations hook_update_N().
 */

/**
 * Replace the "unique" index with a plain one.
 */
function features_menu_uuid_update_7100() {
  // Remove the "unique" index.
  if (db_index_exists('menu_links', 'menu_links_uuid_key')) {
    db_drop_unique_key('menu_links', 'menu_links_uuid_key');
  }

  // Add the regular index.
  if (!db_index_exists('menu_links', 'uuid')) {
    db_add_index('menu_links', 'uuid', array('uuid'));
  }
}
