Features Menu UUID
------------------
This module provides an alternative system for exporting menu items using the
Features module [1] that aims to be more reliable than alternative options.

The module was based on Entity Menu Links [2], boiled down to the barebones
menu_links functionality - no Entity API integration, no revisions, no Services
integration; then the Features module's features.menu.inc file was copied &
reworked to suit.


Requirements
--------------------------------------------------------------------------------
The following modules are required in order for this module to work:
* Features - https://www.drupal.org/project/features
  Version 7.x-2.11 or newer is recommended, but this should work with any of the
  v7x-2.x releases.
* CTools - https://www.drupal.org/project/ctools
  While version 7.x-1.15 or newer is recommended, version 7.x-1.4 is required.

Optional:
* UUID - https://www.drupal.org/project/uuid
  Allows referencing menu paths using their UUID values
  (uuid/node/491d01cd-4b3d-4cf2-8d27-83e5d2370b17) instead of the normal system
  paths (node/123).


Usage
--------------------------------------------------------------------------------
Using this module, all menu items will be exportable using the Features module,
listed in the "Menu links (with UUID)" component group.

Note: When this module is first installed it will add a new "uuid" column to the
'menu_links' table and then fill in a value for every record. Should this
process fail, simply disable the module and enable it again to let it finish
saving the values.


Known issues
--------------------------------------------------------------------------------
If Entity Menu Links is also installed, Features Menu UUID will attempt to avoid
problems, specifically:
* Skip its custom logic when menu items are created or updated, because EML will
  already assign a UUID value.
* Skips deleting the 'uuid' column from the 'menu_links' table when uninstalling
  FMU, because EML will still need.

Because of the above, it should be safe to run Features Menu UUID on the same
site as Entity Menu Links.


Credits / contact
--------------------------------------------------------------------------------
Written and maintained by Damien McKenna [3] based on code by Chad DeGroot [4],
James Harvey [5]. and the Features community.

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project's issue queue:
  https://www.drupal.org/project/issues/features_menu_uuid


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/project/features
2: https://www.drupal.org/project/entity_menu_links
3: https://www.drupal.org/u/damienmckenna
4: https://www.drupal.org/u/badjava
5: https://www.drupal.org/u/jamesharv
