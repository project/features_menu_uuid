<?php

/**
 * @file
 * Base class for the features Menu UUID module's tests.
 */

/**
 * Base class for the features Menu UUID module's tests.
 */
abstract class FeaturesMenuUuidTestBase extends DrupalWebTestCase {

  /**
   * Set up test.
   */
  public function setUp(array $modules = array()) {
    $modules[] = 'features_menu_uuid';
    parent::setUp($modules);

    // Run a features rebuild to ensure our feature is fully installed.
    features_rebuild();

    // Skip concerns with permissions, just log in as user 1.
    $this->loginUser1();
  }

  /**
   * Log in as user 1.
   */
  public function loginUser1() {
    $password = user_password();

    // Reset the user 1 password.
    $account = user_load(1);
    $edit = array(
      'pass' => $password,
    );
    $account = user_save($account, $edit);
    $account->pass_raw = $password;

    // Log in as user 1.
    $this->drupalLogin($account);
  }

  /**
   * {@inheritdoc}
   */
  public function verbose($message, $title = NULL) {
    // Handle arrays, objects, etc.
    if (!is_string($message)) {
      $message = "<pre>\n" . print_r($message, TRUE) . "\n</pre>\n";
    }

    // Optional title to go before the output.
    if (!empty($title)) {
      $title = '<h2>' . check_plain($title) . "</h2>\n";
    }

    parent::verbose($title . $message);
  }

  /**
   * Provide a list of menu items.
   *
   * @param int $count
   *   The number of menu items to return.
   *
   * @return array
   *   A list of menu item UUIDs.
   */
  public function someMenuItems($count = 5) {
    $all_items = menu_links_uuid_features_export_options();
    $some_items = array_slice($all_items, 0, $count);
    $this->verbose($some_items);
    $selected_items = array();
    foreach (array_keys($some_items) as $item) {
      $selected_items[$item] = $item;
    }
    $this->verbose($selected_items);

    return $selected_items;
  }

  /**
   * Find a menu item based upon its title.
   *
   * @param string $link_title
   *   The title string to search for.
   * @param string $menu_name
   *   The 'menu_name' value from the {menu_items} table; defaults to
   *   'main-menu' so it loads from the main menu.
   *
   * @return false|array
   *   The menu item if found, otherwise FALSE.
   */
  public function findMenuItem($link_title, $menu_name = 'main-menu') {
    $links = db_select('menu_links')
      ->fields('menu_links')
      ->condition('menu_name', $menu_name)
      ->condition('link_title', $link_title)
      ->execute()
      ->fetchAssoc();
    if (!empty($links)) {
      return $links;
    }
    else {
      return FALSE;
    }
  }

}
