<?php

/**
 * @file
 * Confirm that menu items are exportable.
 */

/**
 * Confirm that menu items are exportable and importable.
 */
class FeaturesMenuUuidTestCase extends FeaturesMenuUuidTestBase {

  /**
   * Test info.
   */
  public static function getInfo() {
    return array(
      'name' => t('Features Menu UUID'),
      'description' => t('Verify that menu items can be imported and exported using UUID values will work.') ,
      'group' => 'features_menu_uuid',
      'dependencies' => array('ctools', 'features'),
    );
  }

  /**
   * Confirm that the Features integration was found.
   */
  public function testFeaturesIntegrationFound() {
    // Check whether the module's Features integration was found.
    $features_apis = features_get_components();
    $this->verbose($features_apis);
    $this->assertTrue(!empty($features_apis['menu_links_uuid']));
  }

  /**
   * Confirm that component was found.
   */
  public function testComponentIsAvailable() {
    // Load the Features admin file, which contains some necessary logic.
    module_load_include('inc', 'features', 'features.admin');

    // Need to pass in some legit variables for the Features system.
    $feature = new StdClass();
    $form_state = array();
    $components = _features_export_build($feature, $form_state);
    $this->verbose($components);

    // Verify that the expected values are present.
    $this->assertTrue(isset($components['components']['menu_links_uuid']));
    $this->assertTrue(isset($components['components']['menu_links_uuid']));
    $this->assertTrue(isset($components['components']['menu_links_uuid']['options']));
    $this->assertTrue(isset($components['components']['menu_links_uuid']['options']['sources']));
    $this->assertTrue(!empty($components['components']['menu_links_uuid']['options']['sources']));
  }

  /**
   * Confirm that menu items are exportable.
   */
  public function testDataToSelect() {
    // Load all menu items.
    $items = menu_links_uuid_features_export_options();
    $this->verbose($items);

    // Confirm the array contains a list of menu items keyed by UUID.
    $this->assertTrue(count($items) > 0, 'Menu items were found.');
    $every_item_has_uuid_key = TRUE;
    foreach ($items as $key => $val) {
      if (empty($key)) {
        $every_item_has_uuid_key = FALSE;
      }
    }
    $this->assertTrue($every_item_has_uuid_key, 'All menu items have a UUID value.');
  }

  /**
   * Confirm the menu items will be added to the features module's info file.
   */
  public function testInfoFileData() {
    $export = array();

    // Get a list of all menu item IDs.
    $items = $this->someMenuItems();

    // Run an export on the selected items.
    $pipe = menu_links_uuid_features_export($items, $export);
    $this->verbose($export);
    $this->verbose($pipe);

    // This module is expected to be added as a depedency.
    $this->assertTrue(isset($export['dependencies']['features_menu_uuid']));

    // Confirm that items were actually added to the list.
    $this->assertTrue(!empty($export['features']['menu_links_uuid']));

    // There shouldn't be any additional items added to the pipe.
    $this->assertTrue(empty($pipe));

    // Make sure that the same number of items were exported as were selected.
    $this->assertEqual(count($export['features']['menu_links_uuid']), count($items));
  }

  /**
   * Confirm the menu items will be exported properly.
   */
  public function testExportedData() {
    $menu_items = $this->someMenuItems();
    $this->verbose($menu_items);
    $output = menu_links_uuid_features_export_render('a_test', $menu_items);
    $this->verbose($output);

    // Confirm the exported data exists.
    $this->assertTrue(!empty($output['menu_links_uuid']), 'Exported data was found.');

    // Confirm each menu item is in the export.
    foreach ($menu_items as $menu_item_id) {
      $this->assertTrue(strpos($output['menu_links_uuid'], $menu_item_id) !== FALSE);
    }
  }

  /**
   * Confirm the menu items will be imported properly.
   */
  public function testImportedData() {
    // Confirm the menu links are not present before the example feature is
    // enabled.
    $this->drupalGet('admin/structure/menu');
    $this->assertResponse(200);
    $this->assertNoText('Test menu');
    $this->drupalGet('admin/structure/menu/manage/menu-test-menu');
    $this->assertResponse(404);

    // Enable the test feature.
    module_enable(array('features_menu_uuid_test'));
    features_rebuild();

    // Confirm the custom menu is now present after the example feature is
    // enabled.
    $this->drupalGet('admin/structure/menu');
    $this->assertResponse(200);
    $this->assertText('Test menu');
    $this->drupalGet('admin/structure/menu/manage/menu-test-menu');
    $this->assertResponse(200);

    // Load the test menu tree.
    $tree = menu_tree_all_data('menu-test-menu');
    $this->verbose($tree);

    // Confirm each of the menu items is present an in the the correct
    // structure. It should be like this:
    //
    // @code
    // Fruit
    // -> Colors
    // -> -> Green
    // -> -> Red
    // -> Shapes
    // -> -> Cylindrical
    // -> -> Spherical
    // Vegetables
    // -> Colors
    // -> -> Green
    // -> -> Red
    // -> Shapes
    // -> -> Cylindrical
    // -> -> Spherical
    // @endcode
    $this->assertEqual(count($tree), 2, 'There are two parent items in the menu tree.');

    // Explore the first segment of the tree.
    $first = array_shift($tree);
    $this->assertEqual($first['link']['link_title'], 'Fruit', 'Found the Fruit menu item.');
    $this->assertEqual(count($first['below']), 2, 'The Fruit menu has two sub items.');
    $child1 = array_shift($first['below']);
    $this->assertEqual($child1['link']['link_title'], 'Colors', 'Found the Colors menu item.');
    $this->assertEqual(count($child1['below']), 2, 'The Colors menu has two sub items.');
    $grandchild1 = array_shift($child1['below']);
    $this->assertEqual($grandchild1['link']['link_title'], 'Green', 'Found the Green menu item.');
    $this->assertEqual(count($grandchild1['below']), 0, 'The Green menu has no sub items.');
    $grandchild2 = array_shift($child1['below']);
    $this->assertEqual($grandchild2['link']['link_title'], 'Red', 'Found the Red menu item.');
    $this->assertEqual(count($grandchild2['below']), 0, 'The Red menu has no sub items.');
    $child2 = array_shift($first['below']);
    $this->assertEqual($child2['link']['link_title'], 'Shapes', 'Found the Shapes menu item.');
    $this->assertEqual(count($child2['below']), 2, 'The Shapes menu has two sub items.');
    $grandchild1 = array_shift($child2['below']);
    $this->assertEqual($grandchild1['link']['link_title'], 'Cylindrical', 'Found the Cylindrical menu item.');
    $this->assertEqual(count($grandchild1['below']), 0, 'The Cylindrical menu has two sub items.');
    $grandchild2 = array_shift($child2['below']);
    $this->assertEqual($grandchild2['link']['link_title'], 'Spherical', 'Found the Spherical menu item.');
    $this->assertEqual(count($grandchild2['below']), 0, 'The Spherical menu has two sub items.');

    // Explore the second segment of the tree.
    $second = array_shift($tree);
    $this->assertEqual($second['link']['link_title'], 'Vegetables', 'Found the Vegetables menu item.');
    $this->assertEqual(count($second['below']), 2, 'The Vegetables menu has two sub items.');
    $child1 = array_shift($second['below']);
    $this->assertEqual($child1['link']['link_title'], 'Colors', 'Found the Colors menu item.');
    $this->assertEqual(count($child1['below']), 2, 'The Colors menu has two sub items.');
    $grandchild1 = array_shift($child1['below']);
    $this->assertEqual($grandchild1['link']['link_title'], 'Green', 'Found the Green menu item.');
    $this->assertEqual(count($grandchild1['below']), 0, 'The Green menu has no sub items.');
    $grandchild2 = array_shift($child1['below']);
    $this->assertEqual($grandchild2['link']['link_title'], 'Red', 'Found the Red menu item.');
    $this->assertEqual(count($grandchild2['below']), 0, 'The Red menu has no sub items.');
    $child2 = array_shift($second['below']);
    $this->assertEqual($child2['link']['link_title'], 'Shapes', 'Found the Shapes menu item.');
    $this->assertEqual(count($child2['below']), 2, 'The Shapes menu has two sub items.');
    $grandchild1 = array_shift($child2['below']);
    $this->assertEqual($grandchild1['link']['link_title'], 'Cylindrical', 'Found the Cylindrical menu item.');
    $this->assertEqual(count($grandchild1['below']), 0, 'The Cylindrical menu has two sub items.');
    $grandchild2 = array_shift($child2['below']);
    $this->assertEqual($grandchild2['link']['link_title'], 'Spherical', 'Found the Spherical menu item.');
    $this->assertEqual(count($grandchild2['below']), 0, 'The Spherical menu has two sub items.');
  }

  /**
   * Ensure that menu items can be added correctly.
   */
  public function testAddingMenuItem() {
    // Load the test feature so that the menu structure is availabe.
    module_enable(array('features_menu_uuid_test'));
    features_rebuild();

    // Create a menu item.
    $this->drupalGet('admin/structure/menu/manage/menu-test-menu/add');
    $this->assertResponse(200);
    $edit = array(
      'link_title' => 'Mango',
      'link_path' => 'https://www.drupal.org/mango',
    );
    $this->drupalPost(NULL, $edit, 'Save');

    // Confirm the menu item was added correctly.
    $this->assertResponse(200);
    $this->assertText('Your configuration has been saved.');
    $this->assertLink('Mango');
    $this->assertLinkByHref('https://www.drupal.org/mango');

    // Confirm the menu item has a UUID value.
    $menu_item1 = $this->findMenuItem('Mango', 'menu-test-menu');
    $this->verbose($menu_item1);
    $this->assertTrue(!empty($menu_item1));
    $this->assertTrue(!empty($menu_item1['uuid']));

    // Try editing the menu item.
    $this->drupalGet('admin/structure/menu/item/' . $menu_item1['mlid'] . '/edit');
    $this->assertResponse(200);
    $edit = array(
      'link_title' => 'Mangos',
    );
    $this->drupalPost(NULL, $edit, 'Save');
    $this->assertResponse(200);
    $this->assertText('Your configuration has been saved.');
    $this->assertLink('Mangos');
    $this->assertLinkByHref('https://www.drupal.org/mango');

    // Confirm the menu item has a UUID value.
    $menu_item2 = $this->findMenuItem('Mangos', 'menu-test-menu');
    $this->verbose($menu_item2);
    $this->assertTrue(!empty($menu_item2));
    $this->assertTrue(!empty($menu_item2['uuid']));

    // Make sure the menu link's UUID didn't change.
    $this->assertEqual($menu_item1['uuid'], $menu_item2['uuid'], "The menu item's UUID value is still the same.");
  }

  /**
   * Ensure that menu items can be correctly added to nodes.
   */
  public function testAddingLinkToNode() {
    // Try adding a node with a menu item attached.
    $args = array(
      'title' => 'Test page',
      'menu' => array(
        'enabled' => TRUE,
        'link_title' => 'Test menu link',
        'description' => '',
        "menu_name" => 'main-menu',
      ),
    );
    $node = $this->drupalCreateNode($args);
    $this->verbose($node);

    // Confirm the node saved correctly and that it has a menu item.
    $this->assertTrue(!empty($node));
    $this->assertTrue(isset($node->menu['link_title']));
    $this->assertEqual($node->menu['link_title'], $args['menu']['link_title']);

    // Confirm the menu item has a UUID value.
    $menu_item1 = $this->findMenuItem('Test menu link');
    $this->verbose($menu_item1);
    $this->assertTrue(!empty($menu_item1));
    $this->assertTrue(!empty($menu_item1['uuid']));

    // Try editing the node, including editing the menu link.
    $this->drupalGet('node/' . $node->nid . '/edit');
    $edit = array(
      'title' => 'A test page',
      'menu[link_title]' => 'The test menu link',
    );
    $this->drupalPost(NULL, $edit, 'Save');

    // Confirm the menu item still has a UUID value.
    $menu_item2 = $this->findMenuItem('The test menu link');
    $this->verbose($menu_item2);
    $this->assertTrue(!empty($menu_item2));
    $this->assertTrue(!empty($menu_item2['uuid']));

    // Make sure the menu link's UUID didn't change.
    $this->assertEqual($menu_item1['uuid'], $menu_item2['uuid'], "The menu item's UUID value is still the same.");
  }

}
