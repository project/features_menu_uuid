<?php

/**
 * @file
 * features_menu_uuid_test.features.menu_links_uuid.inc
 */

/**
 * Implements hook_menu_links_uuid().
 */
function features_menu_uuid_test_menu_links_uuid() {
  $menu_links_uuid = array();

  // Exported menu link: 18046b81-2254-4552-aafd-1377a4cfa349.
  $menu_links_uuid['18046b81-2254-4552-aafd-1377a4cfa349'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/shapes/spherical',
    'router_path' => '',
    'link_title' => 'Spherical',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => '18046b81-2254-4552-aafd-1377a4cfa349',
    'parent_uuid' => '9068ccde-8ae3-41dd-be82-f823adc06c49',
  );
  // Exported menu link: 1de31c80-6d44-47f9-b710-0c0d864cf8be.
  $menu_links_uuid['1de31c80-6d44-47f9-b710-0c0d864cf8be'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/colors/green',
    'router_path' => '',
    'link_title' => 'Green',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => '1de31c80-6d44-47f9-b710-0c0d864cf8be',
    'parent_uuid' => '8da5174b-21d7-47e3-a3ae-c3d2062e92be',
  );
  // Exported menu link: 2cd0a628-482c-464f-a28c-8fdb88cdc1e9.
  $menu_links_uuid['2cd0a628-482c-464f-a28c-8fdb88cdc1e9'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/colors',
    'router_path' => '',
    'link_title' => 'Colors',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => '2cd0a628-482c-464f-a28c-8fdb88cdc1e9',
    'parent_uuid' => '73d9f9bb-7106-4028-802d-48d9f6bc8e33',
  );
  // Exported menu link: 39891a00-5d07-4c28-a661-d28baecffe65.
  $menu_links_uuid['39891a00-5d07-4c28-a661-d28baecffe65'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/colors/green',
    'router_path' => '',
    'link_title' => 'Green',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => '39891a00-5d07-4c28-a661-d28baecffe65',
    'parent_uuid' => '2cd0a628-482c-464f-a28c-8fdb88cdc1e9',
  );
  // Exported menu link: 6ca606a5-bd5b-465d-ab58-8857f1e6f59c.
  $menu_links_uuid['6ca606a5-bd5b-465d-ab58-8857f1e6f59c'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/shapes',
    'router_path' => '',
    'link_title' => 'Shapes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => '6ca606a5-bd5b-465d-ab58-8857f1e6f59c',
    'parent_uuid' => '73d9f9bb-7106-4028-802d-48d9f6bc8e33',
  );
  // Exported menu link: 6d6cd8bb-2e61-42bb-b228-1a1eee116a9b.
  $menu_links_uuid['6d6cd8bb-2e61-42bb-b228-1a1eee116a9b'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/shapes/cylindrical',
    'router_path' => '',
    'link_title' => 'Cylindrical',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => '6d6cd8bb-2e61-42bb-b228-1a1eee116a9b',
    'parent_uuid' => '9068ccde-8ae3-41dd-be82-f823adc06c49',
  );
  // Exported menu link: 73d9f9bb-7106-4028-802d-48d9f6bc8e33.
  $menu_links_uuid['73d9f9bb-7106-4028-802d-48d9f6bc8e33'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit',
    'router_path' => '',
    'link_title' => 'Fruit',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'uuid' => '73d9f9bb-7106-4028-802d-48d9f6bc8e33',
  );
  // Exported menu link: 82233f36-e0ed-44db-93d1-0ef849f01aad.
  $menu_links_uuid['82233f36-e0ed-44db-93d1-0ef849f01aad'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables',
    'router_path' => '',
    'link_title' => 'Vegetables',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'uuid' => '82233f36-e0ed-44db-93d1-0ef849f01aad',
  );
  // Exported menu link: 86575d3b-b9eb-4607-a0bc-34c57beeb33f.
  $menu_links_uuid['86575d3b-b9eb-4607-a0bc-34c57beeb33f'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/shapes/spherical',
    'router_path' => '',
    'link_title' => 'Spherical',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => '86575d3b-b9eb-4607-a0bc-34c57beeb33f',
    'parent_uuid' => '6ca606a5-bd5b-465d-ab58-8857f1e6f59c',
  );
  // Exported menu link: 8da5174b-21d7-47e3-a3ae-c3d2062e92be.
  $menu_links_uuid['8da5174b-21d7-47e3-a3ae-c3d2062e92be'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/colors',
    'router_path' => '',
    'link_title' => 'Colors',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => '8da5174b-21d7-47e3-a3ae-c3d2062e92be',
    'parent_uuid' => '82233f36-e0ed-44db-93d1-0ef849f01aad',
  );
  // Exported menu link: 9068ccde-8ae3-41dd-be82-f823adc06c49.
  $menu_links_uuid['9068ccde-8ae3-41dd-be82-f823adc06c49'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/shapes',
    'router_path' => '',
    'link_title' => 'Shapes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => '9068ccde-8ae3-41dd-be82-f823adc06c49',
    'parent_uuid' => '82233f36-e0ed-44db-93d1-0ef849f01aad',
  );
  // Exported menu link: 9c001791-4c58-4a29-945f-d4bdbdd69a4f.
  $menu_links_uuid['9c001791-4c58-4a29-945f-d4bdbdd69a4f'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/colors/red',
    'router_path' => '',
    'link_title' => 'Red',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => '9c001791-4c58-4a29-945f-d4bdbdd69a4f',
    'parent_uuid' => '2cd0a628-482c-464f-a28c-8fdb88cdc1e9',
  );
  // Exported menu link: d25325c1-35d1-4cc1-bf43-710586c2671b.
  $menu_links_uuid['d25325c1-35d1-4cc1-bf43-710586c2671b'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/fruit/shapes/cylindrical',
    'router_path' => '',
    'link_title' => 'Cylindrical',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'uuid' => 'd25325c1-35d1-4cc1-bf43-710586c2671b',
    'parent_uuid' => '6ca606a5-bd5b-465d-ab58-8857f1e6f59c',
  );
  // Exported menu link: e95329bc-216a-49c2-878f-848de197e07f.
  $menu_links_uuid['e95329bc-216a-49c2-878f-848de197e07f'] = array(
    'menu_name' => 'menu-test-menu',
    'link_path' => 'https://www.drupal.org/vegetables/colors/red',
    'router_path' => '',
    'link_title' => 'Red',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'uuid' => 'e95329bc-216a-49c2-878f-848de197e07f',
    'parent_uuid' => '8da5174b-21d7-47e3-a3ae-c3d2062e92be',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Colors');
  t('Cylindrical');
  t('Fruit');
  t('Green');
  t('Red');
  t('Shapes');
  t('Spherical');
  t('Vegetables');

  return $menu_links_uuid;
}
