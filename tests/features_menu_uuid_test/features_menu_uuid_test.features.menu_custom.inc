<?php

/**
 * @file
 * features_menu_uuid_test.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function features_menu_uuid_test_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-test-menu.
  $menus['menu-test-menu'] = array(
    'menu_name' => 'menu-test-menu',
    'title' => 'Test menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Test menu');

  return $menus;
}
