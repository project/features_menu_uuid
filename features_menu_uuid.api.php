<?php

/**
 * @file
 * Hooks provided by the Features Menu UUID module.
 */

/**
 * Provide default menu items.
 *
 * @return array
 *   Menu items.
 *
 * @see \hook_menu_default_menu_links()
 */
function hook_menu_links_uuid() {
  return array();
}

/**
 * Alter the default menu links right before they are cached into the database.
 *
 * @param array[] &$links
 *   By reference. The menu links that have been declared by another feature.
 *
 * @see \hook_menu_default_menu_links_alter()
 */
function hook_menu_links_uuid_alter(array &$links) {
}
