Features_Menu_UUID 7.x-1.x-dev, xxxx-xx-xx
------------------------------------------


Features_Menu_UUID 7.x-1.2, 2020-03-07
--------------------------------------
#3118489 by DamienMcKenna: Use a regular index, not 'unique' one.


Features_Menu_UUID 7.x-1.1, 2020-03-07
--------------------------------------
#3117683 by DamienMcKenna: Fix tests in 7.x-1.x branch.
#3118248 by DamienMcKenna: features_menu_uuid_menu_link_update() uses
  uuid_generate() not ctools_uuid_generate().
#3118252 by DamienMcKenna: Tidy up features_menu_uuid_menu_link_insert() and
  features_menu_uuid_menu_link_update().
#3118254 by DamienMcKenna: Add test coverage for adding a menu item.
#3118255 by DamienMcKenna: Add test coverage for editing a menu item.


Features_Menu_UUID 7.x-1.0, 2020-03-04
--------------------------------------
By DamienMcKenna: Initial version.
