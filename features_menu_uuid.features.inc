<?php

/**
 * @file
 * Features integration for the Features Menu UUID module.
 */

/**
 * Implements hook_features_api().
 */
function features_menu_uuid_features_api() {
  return array(
    // This is similar to the 'menu_links' definition.
    'menu_links_uuid' => array(
      'name' => t('Menu links (with UUID)'),
      /* @see \hook_menu_links_uuid() */
      /* @see \hook_menu_links_uuid_alter() */
      'default_hook' => 'menu_links_uuid',
      'feature_source' => TRUE,
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'file' => drupal_get_path('module', 'features_menu_uuid') . '/features_menu_uuid.features.inc',
    ),
  );
}

/**
 * Implements hook_features_export_options().
 */
function menu_links_uuid_features_export_options() {
  global $menu_admin;
  // Need to set this to TRUE in order to get menu links that the
  // current user may not have access to (i.e. user/login).
  $menu_admin = TRUE;
  $use_menus = array_intersect_key(menu_get_menus(), array_flip(array_filter(variable_get('features_admin_menu_links_menus', array_keys(menu_get_menus())))));
  $menu_links = menu_parent_options($use_menus, array('mlid' => 0));
  $options = array();
  foreach ($menu_links as $key => $name) {
    list($menu_name, $mlid) = explode(':', $key, 2);
    if ($mlid != 0) {
      $link = menu_link_load($mlid);

      // Add a UUID attribute if one not defined.
      if (empty($link['uuid'])) {
        // This doesn't actually work as the menu item then can't be loaded in
        // menu_links_uuid_link_load(), so just fail.
        // @code
        // $link['uuid'] = ctools_uuid_generate();
        // @endcode
        continue;
      }

      // UUID URL handling.
      if (module_exists('uuid')) {
        _menu_links_uuid_set_menu_link_uuid_path($link);
      }

      $options[$link['uuid']] = "{$menu_name}: {$name}";
    }
  }
  $menu_admin = FALSE;
  return $options;
}

/**
 * Implements hook_features_export().
 */
function menu_links_uuid_features_export($data, &$export, $module_name = '') {
  // Default hooks are provided by the feature module so we need to add
  // it as a dependency.
  $export['dependencies']['features_menu_uuid'] = 'features_menu_uuid';

  // Collect a link-to-module map.
  $pipe = array();
  // @todo Is this needed?
  // $map = features_get_default_map('menu_links', 'menu_links_uuid_features_identifier');
  foreach ($data as $identifier) {
    if ($link = menu_links_uuid_link_load($identifier)) {
      // UUID URL handling.
      // Without this, features info file will lose UUID for menu links.
      if (module_exists('uuid')) {
        _menu_links_uuid_set_menu_link_uuid_path($link);
      }

      // If this link is provided by a different module, add it as a dependency.
      // @todo Explain why empty($export) is passed as second parameter.
      // @see #3075693
      // @todo $export is never empty, see initialization above. See #3075693.
      // @code
      // if (isset($map[$identifier]) && $map[$identifier] != $module_name) {
      //   $export['dependencies'][$map[$identifier]] = $map[$identifier];
      // }
      // else {
      // @endcode
      $export['features']['menu_links_uuid'][$link['uuid']] = $link['uuid'];
      // @code
      // }
      // @endcode
    }
  }
  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function menu_links_uuid_features_export_render($module, $data, $export = NULL) {
  $code = array();
  $code[] = '  $menu_links_uuid = array();';
  $code[] = '';

  $translatables = array();
  foreach ($data as $identifier) {
    if ($link = menu_links_uuid_link_load($identifier)) {
      // UUID URL handling.
      if (module_exists('uuid')) {
        _menu_links_uuid_set_menu_link_uuid_path($link);
      }

      // Replace plid with a parent path.
      if (!empty($link['plid']) && $parent = menu_link_load($link['plid'])) {
        // UUID URL handling.
        if (module_exists('uuid')) {
          _menu_links_uuid_set_menu_link_uuid_path($parent);
        }
        $link['parent_uuid'] = $parent['uuid'];
      }

      unset($link['plid']);
      unset($link['mlid']);

      $code[] = "  // Exported menu link: {$link['uuid']}.";
      $code[] = "  \$menu_links_uuid['{$link['uuid']}'] = " . features_var_export($link, '  ') . ";";
      $translatables[] = $link['link_title'];
    }
  }
  $code[] = '';
  if (!empty($translatables)) {
    $code[] = features_translatables_export($translatables, '  ');
  }

  $code[] = '  return $menu_links_uuid;';
  $code = implode("\n", $code);
  /* @see \hook_menu_links_uuid() */
  return array('menu_links_uuid' => $code);
}

/**
 * Implements hook_features_revert().
 */
function menu_links_uuid_features_revert($module) {
  menu_links_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function menu_links_uuid_features_rebuild($module) {
  if ($menu_links = features_get_default('menu_links_uuid', $module)) {
    menu_links_uuid_features_rebuild_ordered($menu_links);
  }
}

/**
 * Generate a depth tree of all menu links.
 *
 * @param array $menu_links
 *   Array of menu links.
 * @param bool $reset
 *   If TRUE, the static cache will be reset.
 */
function menu_links_uuid_features_rebuild_ordered(array $menu_links, $reset = FALSE) {
  static $ordered;
  static $all_links;
  if (!isset($ordered) || $reset) {
    $ordered = array();
    $unordered = features_get_default('menu_links_uuid');

    // Order all links by depth.
    if ($unordered) {
      do {
        $current = count($unordered);
        foreach ($unordered as $key => $link) {
          $identifier = menu_links_uuid_features_identifier($link);
          $parent = isset($link['parent_uuid']) ? $link['parent_uuid'] : '';
          $weight = 0;
          // Parent has been seen, so weigh this above parent.
          if (isset($ordered[$parent])) {
            $weight = $ordered[$parent] + 1;
          }
          // Next loop will try to find parent weight instead.
          elseif ($parent) {
            continue;
          }
          $ordered[$identifier] = $weight;
          $all_links[$identifier] = $link;
          unset($unordered[$key]);
        }
        // Exit out when the above does no changes this loop.
      } while (count($unordered) < $current);
    }
    // Add all remaining unordered items to the ordered list.
    foreach ($unordered as $link) {
      $identifier = menu_links_uuid_features_identifier($link);
      $ordered[$identifier] = 0;
      $all_links[$identifier] = $link;
    }
    asort($ordered);
  }

  // Ensure any default menu items that do not exist are created.
  foreach (array_keys($ordered) as $identifier) {
    $link = $all_links[$identifier];

    $existing = menu_links_uuid_link_load($identifier);
    if (!$existing || in_array($link, $menu_links)) {
      // Restore the internal path instead of the UUID path.
      $internal_path = '';
      $uri = explode('/', $link['link_path']);
      if (module_exists('uuid') && $uri[0] == 'uuid' && count($uri) == 3) {
        $entity_data = uuid_uri_array_to_data($uri);
        $entities = entity_uuid_load($entity_data['entity_type'], array($entity_data['uuid']));
        if (!empty($entities)) {
          $entity = reset($entities);
          $internal_uri = entity_uri($entity_data['entity_type'], $entity);
          $internal_path = $internal_uri['path'];
        }
      }
      $link_paths = array($link['link_path']);
      if (!empty($internal_path)) {
        $link['link_path'] = $internal_path;
      }

      // Retrieve the mlid if this is an existing item.
      if ($existing) {
        $link['mlid'] = $existing['mlid'];
      }
      // Retrieve the plid for a parent link.
      if (!empty($link['parent_uuid']) && $parent = menu_links_uuid_link_load($link['parent_uuid'])) {
        $link['plid'] = $parent['mlid'];
      }
      // This if for backwards compatibility.
      elseif (!empty($link['parent_path']) && $parent = menu_links_uuid_link_load("{$link['menu_name']}:{$link['parent_path']}")) {
        $link['plid'] = $parent['mlid'];
      }
      else {
        $link['plid'] = 0;
      }
      menu_link_save($link);
    }
  }
}

/**
 * Loads a menu link by its UUID identifier.
 *
 * @param string $uuid
 *   A menu item's UUID value.
 *
 * @return array|false
 *   The menu link, or FALSE if not found.
 *
 * @todo Describe behavior for old vs new identifier format. See #3075693.
 */
function menu_links_uuid_link_load($uuid) {
  $links = db_select('menu_links')
    ->fields('menu_links', array(
      'menu_name',
      'mlid',
      'plid',
      'link_path',
      'router_path',
      'link_title',
      'options',
      'module',
      'hidden',
      'external',
      'has_children',
      'expanded',
      'weight',
      'customized',
      'uuid',
    ))
    ->condition('uuid', $uuid)
    ->addTag('features_menu_uuid_link')
    ->execute()
    ->fetchAllAssoc('uuid');

  // Only return the first item.
  if (!empty($links)) {
    $link = reset($links);
    $link->options = unserialize($link->options);
    return (array) $link;
  }

  return FALSE;
}

/**
 * Callback for generating the menu link exportable identifier.
 *
 * @param array $link
 *   A menu link, e.g. from menu_link_load().
 *
 * @return string|false
 *   The identifier, a UUID string.
 *
 * @see \menu_links_uuid_link_load()
 */
function menu_links_uuid_features_identifier(array $link) {
  return $link['uuid'];
}

/**
 * Helper function to set UUID data for menu items.
 *
 * @param array $link
 *   Configuration of a specific menu link.
 */
function _menu_links_uuid_set_menu_link_uuid_path(array &$link) {
  // UUID URL handling.
  if (module_exists('uuid') &&
    in_array(
      $link['router_path'],
      array('node/%', 'taxonomy/term/%', 'user/%', 'uuid'))
  ) {
    $entity_type = 'node';
    switch ($link['router_path']) {
      case 'taxonomy/term/%':
        $entity_type = 'taxonomy_term';
        break;

      case 'user/%':
        $entity_type = 'user';
        break;

      case 'uuid':
        // Restore the internal path instead of the UUID path.
        $uri = explode('/', $link['link_path']);
        if (count($uri) == 3 && $uri[0] == 'uuid') {
          $entity_data = uuid_uri_array_to_data($uri);
          $entities = entity_uuid_load($entity_data['entity_type'], array($entity_data['uuid']));
          if (!empty($entities)) {
            $entity_type = $entity_data['entity_type'];
            $entity = reset($entities);
            $internal_uri = entity_uri($entity_data['entity_type'], $entity);
            $link['link_path'] = $internal_uri['path'];
          }
        }
        // The router path is set to 'uuid', but the link path is not a UUID
        // path.
        else {
          switch ($uri[0]) {
            case 'taxonomy':
              $entity_type = 'taxonomy_term';
              break;

            case 'user':
              $entity_type = 'user';
              break;
          }
        }
        break;
    }
    $path_position = substr_count($link['link_path'], '/');
    $entity = menu_get_object($entity_type, $path_position, $link['link_path']);
    if ($entity && isset($entity->uuid)) {
      $link['link_path'] = 'uuid/' . $entity_type . '/' . $entity->uuid;
      $link['router_path'] = 'uuid';
    }
  }
}
